package com.example.denailly.mytimer.TimerModel;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface SerieDAO {

    @Query("SELECT * FROM serie")
    List<Serie> getAll();

    @Query("SELECT * FROM serie WHERE id=:serieId")
    Serie getById(long serieId);

    @Query("SELECT COUNT(*) FROM serie")
    int count();

    @Query("SELECT * FROM serie WHERE id IN (:serieIds)")
    List<Serie> loadAllByIds(long[] serieIds);

    @Insert
    long insert(Serie serie);

    @Insert
    long[] insert(Serie... series);

    @Update
    void update(Serie serie);

    @Delete
    void delete(Serie serie);

}
