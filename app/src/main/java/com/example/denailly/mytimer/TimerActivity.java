package com.example.denailly.mytimer;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.denailly.mytimer.TimerModel.Cycle;
import com.example.denailly.mytimer.TimerModel.Sequence;
import com.example.denailly.mytimer.TimerModel.Serie;
import com.example.denailly.mytimer.TimerModel.TabataTimer;
import com.example.denailly.mytimer.db.DatabaseClient;

import java.util.Timer;

public class TimerActivity extends AppCompatActivity {

    private static final String STATE_TIMER = "TIMER";

    private DatabaseClient mDb;

    private Sequence timerSequence;
    private Serie timerSerie;
    private Cycle timerCycle;

    private TabataTimer tabataTimer;

    private TextView labelTimerView;
    private TextView timerView;
    private Button timerStartView;
    private ProgressBar timerProgressView;
    private ProgressBar globalProgressView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);

        if (savedInstanceState != null) {
            Log.v("state", savedInstanceState.getStringArrayList(STATE_TIMER).toString());
            //this.tabataTimer = (TabataTimer) savedInstanceState.getSerializable(STATE_TIMER);
        }
        final Sequence sequence = (Sequence) getIntent().getSerializableExtra("sequence");
        loadSequence(sequence);

        // Get the database client
        mDb = DatabaseClient.getInstance(getApplicationContext());

        // Get the views
        this.labelTimerView = findViewById(R.id.label_timer);
        this.timerView = findViewById(R.id.main_timer);
        this.timerStartView = findViewById(R.id.timer_start);
        this.timerProgressView = findViewById(R.id.timer_progress_bar);
        this.globalProgressView = findViewById(R.id.global_progress_bar);

        // Attach click listeners to buttons
        this.timerStartView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Set the first label (preparation) since it won't be called by onFinish callback
                TimerActivity.this.labelTimerView.setText(TimerActivity.this.tabataTimer.getLabels().get(0));
                TimerActivity.this.tabataTimer.start();
                TimerActivity.this.timerStartView.setVisibility(View.INVISIBLE);
            }
        });

        // Prepare the progress bars
        this.timerProgressView.setMax(100);
        this.globalProgressView.setMax(100);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putStringArrayList(STATE_TIMER, this.tabataTimer.getLabels());
        super.onSaveInstanceState(savedInstanceState);
    }

    private void loadSequence(final Sequence sequence) {

        class LoadSequence extends AsyncTask<Void, Void, Sequence> {

            @Override
            protected Sequence doInBackground(Void... voids) {
                // Get the serie and cycle objects to get the full informations
                Serie serie = mDb.getAppDatabase().serieDAO().getById(sequence.getSerieId());
                Cycle cycle = mDb.getAppDatabase().cycleDAO().getById(serie.getCycleId());

                TimerActivity.this.prepareTimer(sequence, serie, cycle);
                return sequence;
            }

            @Override
            protected void onPostExecute(Sequence sequence) {
                super.onPostExecute(sequence);
            }
        }

        LoadSequence ls = new LoadSequence();
        ls.execute();

    }

    private void prepareTimer(Sequence sequence, Serie serie, Cycle cycle) {
        // Assign the given objects to this activity
        this.timerSequence = sequence;
        this.timerSerie = serie;
        this.timerCycle = cycle;

        // Prepare the activity timer object
        // Needs to be run on the UI thread and with a runnable and/or async task
        // see https://stackoverflow.com/questions/4006547/countdowntimer-cant-create-handler-inside-thread-that-has-not-called-looper-p
        this.runOnUiThread(new Runnable() {
                               @Override
                               public void run() {
                                   TimerActivity.this.tabataTimer = new TabataTimer(TimerActivity.this.timerSequence, TimerActivity.this.timerSerie, TimerActivity.this.timerCycle) {

                                       @Override
                                       public void onTick(long millisUntilFinished, final int progress) {
                                           String timer = String.valueOf(millisUntilFinished / 100);
                                           // If there are only milliseconds left, meaning we are under 1 second, append the 0 anyway for UI reasons
                                           // (without this condition, the dot will be the first character)
                                           if (timer.length() == 1) {
                                               timer = "0" + timer;
                                           }
                                           char decimals = timer.charAt(timer.length() - 1);
                                           timer = timer.substring(0, timer.length() - 1) + "." + decimals;
                                           TimerActivity.this.timerView.setText(timer);
                                           TimerActivity.this.timerProgressView.post(new Runnable() {
                                               @Override
                                               public void run() {
                                                   TimerActivity.this.timerProgressView.setProgress(progress);
                                               }
                                           });
                                       }

                                       @Override
                                       public void onGlobalTick(long millisUntilFinished, int progress) {
                                           TimerActivity.this.globalProgressView.setProgress(progress);
                                       }

                                       @Override
                                       public void onFinish(String nextLabel) {
                                           // Empty the timer view since it reached 0 seconds
                                           TimerActivity.this.timerView.setText("");
                                           // Prepare the next label
                                           TimerActivity.this.labelTimerView.setText(nextLabel);
                                       }
                                   };
                               }
                           }
        );
    }

}
