package com.example.denailly.mytimer;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.denailly.mytimer.TimerModel.Sequence;

import java.util.List;

public class SequencesAdapter extends RecyclerView.Adapter<SequencesAdapter.SequencesViewHolder> {

    // DATA
    private Context mCtx;
    private List<Sequence> sequenceList;

    public SequencesAdapter(Context mCtx, List<Sequence> sequenceList) {
        this.mCtx = mCtx;
        this.sequenceList = sequenceList;
    }

    /**
     * Création d'une ligne
     *
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public SequencesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.recyclerview_sequences, parent, false);
        return new SequencesViewHolder(view);
    }

    public void setSequenceList(List<Sequence> sequenceList) {
        this.sequenceList = sequenceList;
    }

    /**
     * Chargement d'une ligne avec les données contenues dans la liste sequenceList à une certaine position
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(SequencesViewHolder holder, int position) {
        Sequence t = sequenceList.get(position);
        holder.textViewName.setText(t.getName());
    }

    @Override
    public int getItemCount() {
        return sequenceList.size();
    }

    /**
     * Classe décrivant le ViewHolder
     * permet de répondre à un événement clique
     */
    class SequencesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        // VIEW
        protected TextView textViewName;

        public SequencesViewHolder(View itemView) {
            super(itemView);

            textViewName = itemView.findViewById(R.id.textViewName);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Sequence sequence = sequenceList.get(getAdapterPosition());
            Intent intent = new Intent(mCtx, TimerActivity.class);
            intent.putExtra("sequence", sequence);

            mCtx.startActivity(intent);
        }

        @Override
        public boolean onLongClick(View view) {
            Sequence sequence = sequenceList.get(getAdapterPosition());

            Intent intent = new Intent(mCtx, EditSequenceActivity.class);
            intent.putExtra("sequence", sequence);

            mCtx.startActivity(intent);
            return true;
        }
    }
}
