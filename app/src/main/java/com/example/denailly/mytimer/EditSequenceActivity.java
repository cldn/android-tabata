package com.example.denailly.mytimer;

import android.arch.persistence.room.Update;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.denailly.mytimer.TimerModel.Cycle;
import com.example.denailly.mytimer.TimerModel.Sequence;
import com.example.denailly.mytimer.TimerModel.Serie;
import com.example.denailly.mytimer.db.DatabaseClient;

public class EditSequenceActivity extends AppCompatActivity {

    private DatabaseClient mDb;

    private EditText nameView;
    private EditText prepView;
    private EditText serieCountView;
    private EditText cycleCountView;
    private EditText workView;
    private EditText restView;
    private EditText serieRestView;
    private EditText cooldownView;
    private Button validateView;
    private Button deleteView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_sequence);

        // Get the database client
        mDb = DatabaseClient.getInstance(getApplicationContext());

        // Get the views
        nameView = findViewById(R.id.edit_sequence_name_value);
        prepView = findViewById(R.id.edit_sequence_prep_value);
        serieCountView = findViewById(R.id.edit_sequence_serie_count_value);
        cycleCountView = findViewById(R.id.edit_sequence_cycle_count_value);
        workView = findViewById(R.id.edit_sequence_work_value);
        restView = findViewById(R.id.edit_sequence_rest_value);
        serieRestView = findViewById(R.id.edit_sequence_serie_rest_value);
        cooldownView = findViewById(R.id.edit_sequence_cooldown_value);
        validateView = findViewById(R.id.edit_sequence_validate);
        deleteView = findViewById(R.id.edit_sequence_delete);

        // Get the current sequence in the intent parameters
        final Sequence sequence = (Sequence) getIntent().getSerializableExtra("sequence");
        loadSequence(sequence);

        validateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateSequence(sequence);
            }
        });

        final AlertDialog.Builder builder = new AlertDialog.Builder(EditSequenceActivity.this);
        builder.setMessage("Are you sure you want to delete this sequence ?")
                .setTitle("Delete sequence");
        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // User clicked "Confirm", he confirms the deletion. Call the delete function with the associated sequence
                deleteSequence(sequence);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // User clicked "Cancel", he chose to stop the deletion process. No feedback to send
                // Toast.makeText(getApplicationContext(), "Clicked cancel", Toast.LENGTH_LONG).show();
            }
        });

        deleteView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    private void loadSequence(final Sequence sequence) {

        class LoadSequence extends AsyncTask<Void, Void, Sequence> {

            @Override
            protected Sequence doInBackground(Void... voids) {

                // Get the serie and cycle objects to get the full informations
                Serie serie = mDb.getAppDatabase().serieDAO().getById(sequence.getSerieId());
                Cycle cycle = mDb.getAppDatabase().cycleDAO().getById(serie.getCycleId());

                // Update the view with the sequence data
                nameView.setText(sequence.getName());
                prepView.setText(Integer.toString(sequence.getPrepTime()));
                serieCountView.setText(Integer.toString(sequence.getSerieRepetition()));
                cycleCountView.setText(Integer.toString(serie.getCycleRepetition()));
                workView.setText(Integer.toString(cycle.getWorkTime()));
                restView.setText(Integer.toString(cycle.getRestTime()));
                serieRestView.setText(Integer.toString(serie.getRestTime()));
                cooldownView.setText(Integer.toString(sequence.getCooldownTime()));

                return sequence;
            }

            @Override
            protected void onPostExecute(Sequence sequence) {
                super.onPostExecute(sequence);
            }
        }

        LoadSequence ls = new LoadSequence();
        ls.execute();
    }

    private void updateSequence(final Sequence sequence) {

        // Every field is required - check that they are correctly filled
        if (nameView.getText().toString().isEmpty()) {
            nameView.setError("Name required");
            nameView.requestFocus();
            return;
        }

        if (prepView.getText().toString().isEmpty()) {
            prepView.setError("Prep required");
            prepView.requestFocus();
            return;
        }

        if (serieCountView.getText().toString().isEmpty()) {
            serieCountView.setError("Serie count required");
            serieCountView.requestFocus();
            return;
        }

        if (cycleCountView.getText().toString().isEmpty()) {
            cycleCountView.setError("Cycle count required");
            cycleCountView.requestFocus();
            return;
        }

        if (workView.getText().toString().isEmpty()) {
            workView.setError("Work required");
            workView.requestFocus();
            return;
        }

        if (restView.getText().toString().isEmpty()) {
            restView.setError("Rest required");
            restView.requestFocus();
            return;
        }

        if (serieRestView.getText().toString().isEmpty()) {
            serieRestView.setError("Serie rest required");
            serieRestView.requestFocus();
            return;
        }

        if (cooldownView.getText().toString().isEmpty()) {
            cooldownView.setError("Cooldown required");
            cooldownView.requestFocus();
            return;
        }

        // Get all the form field values
        final String name = nameView.getText().toString().trim();
        final Integer prep = Integer.valueOf(prepView.getText().toString());
        final Integer serieCount = Integer.valueOf((serieCountView.getText().toString()));
        final Integer cycleCount = Integer.valueOf((cycleCountView.getText().toString()));
        final Integer work = Integer.valueOf(workView.getText().toString());
        final Integer rest = Integer.valueOf(restView.getText().toString());
        final Integer serieRest = Integer.valueOf(serieRestView.getText().toString());
        final Integer cooldown = Integer.valueOf(cooldownView.getText().toString());

        /**
         * Création d'une classe asynchrone pour mettre à jour la séquence
         */
        class UpdateSequence extends AsyncTask<Void, Void, Sequence> {

            @Override
            protected Sequence doInBackground(Void... voids) {

                // Get the serie and cycle objects to update them too
                Serie serie = mDb.getAppDatabase().serieDAO().getById(sequence.getSerieId());
                Cycle cycle = mDb.getAppDatabase().cycleDAO().getById(serie.getCycleId());

                // Update the local objects
                cycle.setRestTime(rest);
                cycle.setWorkTime(work);
                serie.setRestTime(serieRest);
                serie.setCycleRepetition(cycleCount);
                sequence.setName(name);
                sequence.setPrepTime(prep);
                sequence.setCooldownTime(cooldown);
                sequence.setSerieRepetition(serieCount);

                // Push them to the database
                mDb.getAppDatabase().cycleDAO().update(cycle);
                mDb.getAppDatabase().serieDAO().update(serie);
                mDb.getAppDatabase().sequenceDAO().update(sequence);

                return sequence;
            }

            @Override
            protected void onPostExecute(Sequence sequence) {
                super.onPostExecute(sequence);

                // The sequence is successfully updated, we can finish the current activity
                finish();
                Toast.makeText(getApplicationContext(), "Séquence mise à jour", Toast.LENGTH_LONG).show();
            }
        }

        UpdateSequence us = new UpdateSequence();
        us.execute();
    }

    private void deleteSequence(final Sequence sequence) {
        class DeleteSequence extends AsyncTask<Void, Void, Sequence> {

            @Override
            protected Sequence doInBackground(Void... voids) {

                // Get the serie and cycle objects to delete them too
                Serie serie = mDb.getAppDatabase().serieDAO().getById(sequence.getSerieId());
                Cycle cycle = mDb.getAppDatabase().cycleDAO().getById(serie.getCycleId());

                mDb.getAppDatabase().sequenceDAO().delete(sequence);
                mDb.getAppDatabase().serieDAO().delete(serie);
                mDb.getAppDatabase().cycleDAO().delete(cycle);

                return sequence;
            }

            @Override
            protected void onPostExecute(Sequence sequence) {
                super.onPostExecute(sequence);

                // The sequence is successfully deleted, we can finish the current activity
                finish();
                Toast.makeText(getApplicationContext(), "Séquence supprimée", Toast.LENGTH_LONG).show();
            }
        }

        DeleteSequence ds = new DeleteSequence();
        ds.execute();
    }
}
