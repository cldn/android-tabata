package com.example.denailly.mytimer.TimerModel;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "serie",
        foreignKeys = @ForeignKey(entity = Cycle.class,
                parentColumns = "id",
                childColumns = "cycle_id"))
public class Serie {

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "cycle_id")
    private long cycleId;

    @ColumnInfo(name = "cycle_repetition")
    private int cycleRepetition;

    @ColumnInfo(name = "rest")
    private int restTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCycleId() {
        return cycleId;
    }

    public void setCycleId(long cycleId) {
        this.cycleId = cycleId;
    }

    public int getCycleRepetition() {
        return cycleRepetition;
    }

    public void setCycleRepetition(int cycleRepetition) {
        this.cycleRepetition = cycleRepetition;
    }

    public int getRestTime() {
        return restTime;
    }

    public void setRestTime(int restTime) {
        this.restTime = restTime;
    }
}
