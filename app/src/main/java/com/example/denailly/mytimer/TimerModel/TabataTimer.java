package com.example.denailly.mytimer.TimerModel;

import android.os.CountDownTimer;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;

public abstract class TabataTimer implements Serializable {

    private static final String PREPARATION_LABEL = "Préparez vous..";
    private static final String WORK_LABEL = "Travail";
    private static final String REST_LABEL = "Repos";
    private static final String SERIE_REST_LABEL = "Repos inter-séries";
    private static final String COOLDOWN_LABEL = "Repos de fin";

    private CountDownTimer cdT;
    private CountDownTimer totalTimer;
    private ArrayList<Long> timers;
    private ArrayList<String> labels;


    public TabataTimer(Sequence sequence, Serie serie, Cycle cycle) {
        this.buildTimersList(sequence, serie, cycle);
        this.buildGlobalTimer();
        this.buildTimer(0);
    }

    private void buildTimersList(Sequence sequence, Serie serie, Cycle cycle) {
        ArrayList<Long> timesList = new ArrayList<>();
        ArrayList<String> labelsList = new ArrayList<>();

        timesList.add((long)(sequence.getPrepTime()));
        labelsList.add(PREPARATION_LABEL);
        for (int i = 0; i < sequence.getSerieRepetition(); i++) {
            for (int j = 0; j < serie.getCycleRepetition(); j++) {
                timesList.add((long)(cycle.getWorkTime()));
                labelsList.add(WORK_LABEL);
                timesList.add((long)(cycle.getRestTime()));
                labelsList.add(REST_LABEL);
            }
            timesList.add((long)(serie.getRestTime()));
            labelsList.add(SERIE_REST_LABEL);
        }
        // Replace the last inter-series rest by the sequence cooldown time.
        // (Because it is the last series)
        timesList.set(timesList.size() - 1, (long)(sequence.getCooldownTime()));
        labelsList.set(labelsList.size() - 1, COOLDOWN_LABEL);
        this.setTimers(timesList);
        this.setLabels(labelsList);
    }

    private void buildGlobalTimer() {
        final int totalMillisTimer = this.getCombinedTimer() * 1000;
        this.setTotalTimer(new CountDownTimer(totalMillisTimer, 50) {
            @Override
            public void onTick(long millisUntilFinished) {
                int totalProgress = (int)(100 - (millisUntilFinished * 100) / totalMillisTimer);
                TabataTimer.this.onGlobalTick(millisUntilFinished, totalProgress);
            }

            @Override
            public void onFinish() {
                Log.d("Global timer", "done");
            }
        });
    }

    private void buildTimer(final int timeIndex) {
        final long millisTimer = this.getTimers().get(timeIndex) * 1000;
        this.setCdT(new CountDownTimer(millisTimer, 50) {
            @Override
            public void onTick(long millisUntilFinished) {
                int progress =  (int)(100 - (millisUntilFinished * 100) / millisTimer);
                TabataTimer.this.onTick(millisUntilFinished, progress);
            }

            @Override
            public void onFinish() {
                // If we haven't reached the end of the timers array, run the next one
                String nextLabel;
                if ((timeIndex + 1) < TabataTimer.this.timers.size()) {
                    TabataTimer.this.buildTimer(timeIndex + 1);
                    TabataTimer.this.getCdT().start();
                    nextLabel = TabataTimer.this.getLabels().get(timeIndex + 1);
                } else {
                    nextLabel = "Séquence terminée !";
                }
                TabataTimer.this.onFinish(nextLabel);
            }
        });
    }

    public abstract void onTick(long millisUntilFinished, int progress);

    public abstract void onGlobalTick(long millisUntilFinished, int progress);

    public abstract void onFinish(String nextLabel);

    public int getCombinedTimer() {
        int combinedTimer = 0;
        for (int i = 0; i < this.getTimers().size(); i++) {
            combinedTimer += this.getTimers().get(i);
        }
        return combinedTimer;
    }

    public void start() {
        this.getCdT().start();
        this.getTotalTimer().start();
    }

    public ArrayList<String> getLabels() {
        return labels;
    }

    public void setLabels(ArrayList<String> labels) {
        this.labels = labels;
    }

    public CountDownTimer getCdT() {
        return cdT;
    }

    public void setCdT(CountDownTimer cdT) {
        this.cdT = cdT;
    }

    public CountDownTimer getTotalTimer() {
        return totalTimer;
    }

    public void setTotalTimer(CountDownTimer totalTimer) {
        this.totalTimer = totalTimer;
    }

    public ArrayList<Long> getTimers() {
        return timers;
    }

    public void setTimers(ArrayList<Long> timers) {
        this.timers = timers;
    }
}
