package com.example.denailly.mytimer.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.denailly.mytimer.TimerModel.Cycle;
import com.example.denailly.mytimer.TimerModel.CycleDAO;
import com.example.denailly.mytimer.TimerModel.Sequence;
import com.example.denailly.mytimer.TimerModel.SequenceDAO;
import com.example.denailly.mytimer.TimerModel.Serie;
import com.example.denailly.mytimer.TimerModel.SerieDAO;

@Database(entities = {Sequence.class, Serie.class, Cycle.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract SequenceDAO sequenceDAO();
    public abstract SerieDAO serieDAO();
    public abstract CycleDAO cycleDAO();
}
