package com.example.denailly.mytimer.TimerModel;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface SequenceDAO {

    @Query("SELECT * FROM sequence")
    List<Sequence> getAll();

    @Query("SELECT * FROM sequence WHERE id=:sequenceId")
    Sequence getById(long sequenceId);

    @Query("SELECT COUNT(*) FROM sequence")
    int count();

    @Query("SELECT * FROM sequence WHERE id IN (:sequenceIds)")
    List<Sequence> loadAllByIds(long[] sequenceIds);

    @Insert
    long insert(Sequence sequence);

    @Insert
    long[] insert(Sequence... sequences);

    @Update
    void update(Sequence sequence);

    @Delete
    void delete(Sequence sequence);

}
