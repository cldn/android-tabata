package com.example.denailly.mytimer;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.denailly.mytimer.TimerModel.Cycle;
import com.example.denailly.mytimer.TimerModel.Sequence;
import com.example.denailly.mytimer.TimerModel.Serie;
import com.example.denailly.mytimer.db.DatabaseClient;

public class NewSequenceActivity extends AppCompatActivity {

    private DatabaseClient mDb;

    private EditText nameView;
    private EditText prepView;
    private EditText serieCountView;
    private EditText cycleCountView;
    private EditText workView;
    private EditText restView;
    private EditText serieRestView;
    private EditText cooldownView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_sequence);

        // Get the database client
        mDb = DatabaseClient.getInstance(getApplicationContext());

        // Get the views
        nameView = findViewById(R.id.new_sequence_name_value);
        prepView = findViewById(R.id.new_sequence_prep_value);
        serieCountView = findViewById(R.id.new_sequence_serie_count_value);
        cycleCountView = findViewById(R.id.new_sequence_cycle_count_value);
        workView = findViewById(R.id.new_sequence_work_value);
        restView = findViewById(R.id.new_sequence_rest_value);
        serieRestView = findViewById(R.id.new_sequence_serie_rest_value);
        cooldownView = findViewById(R.id.new_sequence_cooldown_value);
    }

    public void saveSequence(View v) {

        // Every field is required - check that they are correctly filled
        if (nameView.getText().toString().isEmpty()) {
            nameView.setError("Name required");
            nameView.requestFocus();
            return;
        }

        if (prepView.getText().toString().isEmpty()) {
            prepView.setError("Prep required");
            prepView.requestFocus();
            return;
        }

        if (serieCountView.getText().toString().isEmpty()) {
            serieCountView.setError("Serie count required");
            serieCountView.requestFocus();
            return;
        }

        if (cycleCountView.getText().toString().isEmpty()) {
            cycleCountView.setError("Cycle count required");
            cycleCountView.requestFocus();
            return;
        }

        if (workView.getText().toString().isEmpty()) {
            workView.setError("Work required");
            workView.requestFocus();
            return;
        }

        if (restView.getText().toString().isEmpty()) {
            restView.setError("Rest required");
            restView.requestFocus();
            return;
        }

        if (serieRestView.getText().toString().isEmpty()) {
            serieRestView.setError("Serie rest required");
            serieRestView.requestFocus();
            return;
        }

        if (cooldownView.getText().toString().isEmpty()) {
            cooldownView.setError("Cooldown required");
            cooldownView.requestFocus();
            return;
        }

        // Get all the form field values
        final String name = nameView.getText().toString().trim();
        final Integer prep = Integer.valueOf(prepView.getText().toString());
        final Integer serieCount = Integer.valueOf((serieCountView.getText().toString()));
        final Integer cycleCount = Integer.valueOf((cycleCountView.getText().toString()));
        final Integer work = Integer.valueOf(workView.getText().toString());
        final Integer rest = Integer.valueOf(restView.getText().toString());
        final Integer serieRest = Integer.valueOf(serieRestView.getText().toString());
        final Integer cooldown = Integer.valueOf(cooldownView.getText().toString());

        /**
         * Création d'une classe asynchrone pour sauvegarder les données de la séquence
         */
        class SaveSequence extends AsyncTask<Void, Void, Sequence> {

            @Override
            protected Sequence doInBackground(Void... voids) {

                // Create the first item : a cycle
                Cycle cycle = new Cycle();
                cycle.setRestTime(rest);
                cycle.setWorkTime(work);

                // Insert the cycle in the database
                long cycleId = mDb.getAppDatabase()
                        .cycleDAO()
                        .insert(cycle);

                // Create the second item : a serie, using the freshly created cycle
                Serie serie = new Serie();
                serie.setCycleId(cycleId);
                serie.setCycleRepetition(cycleCount);
                serie.setRestTime(serieRest);

                // Insert the serie in the database
                long serieId = mDb.getAppDatabase()
                        .serieDAO()
                        .insert(serie);

                // Create the third and last item : a sequence, using the freshly created serie
                Sequence sequence = new Sequence();
                sequence.setName(name);
                sequence.setPrepTime(prep);
                sequence.setCooldownTime(cooldown);
                sequence.setSerieRepetition(serieCount);
                sequence.setSerieId(serieId);

                // Insert the sequence in the database
                mDb.getAppDatabase()
                        .sequenceDAO()
                        .insert(sequence);

                return sequence;
            }

            @Override
            protected void onPostExecute(Sequence sequence) {
                super.onPostExecute(sequence);

                // The sequence is successfully created, we can finish the current activity
                finish();
                Toast.makeText(getApplicationContext(), "Séquence enregistrée", Toast.LENGTH_LONG).show();
            }
        }

        SaveSequence sq = new SaveSequence();
        sq.execute();
    }
}
