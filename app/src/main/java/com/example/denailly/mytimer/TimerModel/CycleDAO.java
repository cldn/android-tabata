package com.example.denailly.mytimer.TimerModel;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface CycleDAO {

    @Query("SELECT * FROM cycle")
    List<Cycle> getAll();

    @Query("SELECT * FROM cycle WHERE id=:cycleId")
    Cycle getById(long cycleId);

    @Query("SELECT COUNT(*) FROM cycle")
    int count();

    @Query("SELECT * FROM cycle WHERE id IN (:cycleIds)")
    List<Cycle> loadAllByIds(long[] cycleIds);

    @Insert
    long insert(Cycle cycle);

    @Insert
    long[] insert(Cycle... cycles);

    @Update
    void update(Cycle cycle);

    @Delete
    void delete(Cycle cycle);

}
