package com.example.denailly.mytimer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    // Navigate to NewSequenceActivity
    public void onNewSequence(View v) {
        Intent intent = new Intent(this, NewSequenceActivity.class);
        startActivity(intent);
    }

    // Navigate to ViewSequenceActivity
    public void onViewSequences(View v) {
        Intent intent = new Intent(this, ViewSequenceActivity.class);
        startActivity(intent);
    }

    // Navigate to HistoryActivity
    public void onHistory(View v) {
        Intent intent = new Intent(this, HistoryActivity.class);
        startActivity(intent);
    }
}
