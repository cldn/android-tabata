package com.example.denailly.mytimer.TimerModel;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "cycle")
public class Cycle {

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "work")
    private int workTime;

    @ColumnInfo(name = "rest")
    private int restTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getWorkTime() {
        return workTime;
    }

    public void setWorkTime(int workTime) {
        this.workTime = workTime;
    }

    public int getRestTime() {
        return restTime;
    }

    public void setRestTime(int restTime) {
        this.restTime = restTime;
    }
}
