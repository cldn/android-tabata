package com.example.denailly.mytimer;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.denailly.mytimer.TimerModel.Sequence;
import com.example.denailly.mytimer.db.DatabaseClient;

import java.util.ArrayList;
import java.util.List;

public class ViewSequenceActivity extends AppCompatActivity {

    // DATA
    private SequencesAdapter adapter;
    private DatabaseClient mDb;

    // VIEW
    private FloatingActionButton buttonAddSequenceView;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_sequence);

        // Récupération du DatabaseClient
        mDb = DatabaseClient.getInstance(getApplicationContext());

        // Récupérer les vues
        recyclerView = findViewById(R.id.recyclerview_sequences);
        buttonAddSequenceView = findViewById(R.id.floating_button_add);

        // Lier l'adapter au recyclerView
        adapter = new SequencesAdapter(ViewSequenceActivity.this, new ArrayList<Sequence>());
        recyclerView.setAdapter(adapter);

        // Ajouter un événement au bouton d'ajout
        buttonAddSequenceView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ViewSequenceActivity.this, NewSequenceActivity.class);
                startActivity(intent);
            }
        });

    }


    private void getSequences() {

        ///////////////////////
        // Classe asynchrone permettant de récupérer des séquences et de mettre à jour le recyclerView de l'activité
        class GetSequences extends AsyncTask<Void, Void, List<Sequence>> {

            @Override
            protected List<Sequence> doInBackground(Void... voids) {
                List<Sequence> sequenceList = mDb.getAppDatabase()
                        .sequenceDAO()
                        .getAll();
                return sequenceList;
            }

            @Override
            protected void onPostExecute(List<Sequence> sequences) {
                super.onPostExecute(sequences);

                // Mettre à jour l'adapter avec la liste de séquences
                adapter.setSequenceList(sequences);

                // Now, notify the adapter of the change in source
                adapter.notifyDataSetChanged();
            }
        }

        // Création d'un objet de type GetSéquences et execution de la demande asynchrone
        GetSequences gs = new GetSequences();
        gs.execute();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Mise à jour des séquences
        getSequences();
    }
}
