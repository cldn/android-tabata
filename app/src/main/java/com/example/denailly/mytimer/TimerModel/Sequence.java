package com.example.denailly.mytimer.TimerModel;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "sequence",
        foreignKeys = @ForeignKey(entity = Serie.class,
                parentColumns = "id",
                childColumns = "serie_id"))
public class Sequence implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "serie_id")
    private long serieId;

    @ColumnInfo(name = "prep_time")
    private int prepTime;

    @ColumnInfo(name = "serie_repetition")
    private int serieRepetition;

    @ColumnInfo(name = "cooldown_time")
    private int cooldownTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSerieId() {
        return serieId;
    }

    public void setSerieId(long serieId) {
        this.serieId = serieId;
    }

    public int getPrepTime() {
        return prepTime;
    }

    public void setPrepTime(int prepTime) {
        this.prepTime = prepTime;
    }

    public int getSerieRepetition() {
        return serieRepetition;
    }

    public void setSerieRepetition(int serieRepetition) {
        this.serieRepetition = serieRepetition;
    }

    public int getCooldownTime() {
        return cooldownTime;
    }

    public void setCooldownTime(int cooldownTime) {
        this.cooldownTime = cooldownTime;
    }
}
